package com.company;


class Hotel_Bedrooms implements MyInterface {
    private int dimension;
    private int height;
    private int weight;

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }


    public Hotel_Bedrooms(int dimension, int height, int weight) {
        this.dimension = dimension;
        this.height = height;
        this.weight = weight;
    }

    @Override
    public int calculateSize() {
        return dimension * height * weight;

    }

    @Override
    public void getStyle(boolean isThisFirstBed) {
        if (isThisFirstBed) {
            System.out.println("The style of this bed is called Headboard and Side Rails ");
        } else {
            System.out.println("The style of the second bed is Swedish");
        }
    }
}


