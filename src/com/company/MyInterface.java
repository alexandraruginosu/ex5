package com.company;

public interface MyInterface {

    int calculateSize();

    //we need this boolean to discern between the two styles
    void getStyle(boolean isThisFirstBed);

}
